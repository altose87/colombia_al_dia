(function($) {
  $(function() {

    $(window).on('pageinit', '#list', function( evt, ui ) {

        // Initialize the lazyloader widget
        $("#list").lazyloader({
          retrieve: 20,
          retrieved: 20,
          bubbles: true
        });

        /* Set some default options for the lazyloader
         *   the first three set the timeout value for when the widget should check
         *   the current scroll position relative to page height to decide whether
         *   or not to load more yet.  The showprogress option is to specify the
         *   duration of the fadeIn animation for the lazyloaderProgressDiv.
         */
        $.mobile.lazyloader.prototype.timeoutOptions.mousewheel = 300;
        $.mobile.lazyloader.prototype.timeoutOptions.scrollstart = 700;
        $.mobile.lazyloader.prototype.timeoutOptions.scrollstop = 100;
        $.mobile.lazyloader.prototype.timeoutOptions.showprogress = 100;
    });
    
    $( window ).on( "orientationchange", function( event ) {
      if(event.orientation == 'portrait'){
        $('.feature-headline').css('bottom','70px');
      } else {
        $('.feature-headline').css('bottom','100px');
      }
    });

  });
})(jQuery);
